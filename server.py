from crypt import methods
import re
from flask import Flask, render_template
import os
import sqlite3
from datetime import datetime

app = Flask(__name__)

@app.route('/birds', methods=['GET'])
def show_birds():
    db = sqlite3.connect('Weather.db')
    cur = db.cursor()
    #images = os.listdir('./images')
    #print(images)
    records = db.execute('SELECT * FROM Weather ORDER BY date DESC').fetchall();
    table_rows = []
    for record in records:
        date_time = datetime.strptime(record[6], '%Y-%m-%d %H:%M:%S.%f')
        row_items = (record[0], str(round(record[1], 2)), record[2], record[3], record[4], record[5], date_time.strftime('%x'), date_time.strftime('%X'), record[7] )
        table_rows.append(row_items)
    return render_template('images.html', records=table_rows)

if __name__== "__main__":
    app.run(host='0.0.0.0', port=5000)