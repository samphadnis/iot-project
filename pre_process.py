from datetime import timedelta
import time
import csv
import datetime
import pandas as pd

def pre_process():
    df = pd.read_csv('birds.csv', parse_dates=['date'])
    df = df.set_index(['date'])
    df = df.loc['2022-05-06':'2022-05-09']
    
    output = open("birds_cleaned.csv", "w") 

    #id,temperature,humidity,pressure,description,wind,date,image
    current_date = None
    for index, row in df.iterrows():
        diff = None
        if current_date is not None:
            diff = index.to_pydatetime()  - current_date.to_pydatetime()
        if current_date is None or diff.total_seconds() > 10:
            print(str(row['humidity']))
            data = [str(index), str(row['temperature']), str(row['humidity']), str(row['pressure']), str(row['description']), str(row['wind'])]
            line = ','.join(data) + '\n'

            output.write(line)
            current_date = index

    output.close()

if __name__ == "__main__":
	try:
		pre_process()
	finally:
		print("completed")