import argparse
import sys
import time
#from picar_4wd.servo import Servo
#from picar_4wd.ultrasonic import Ultrasonic
#from picar_4wd.pin import Pin
#from picar_4wd.pwm import PWM

import numpy as np
import math as m
import cv2
import heapq
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

from collections import deque

from object_detector import ObjectDetector
from object_detector import ObjectDetectorOptions

from datetime import datetime

import picamera
from PIL import Image
from tflite_runtime.interpreter import Interpreter

import requests, json
import sqlite3
import uuid

#us = Ultrasonic(Pin('D8'), Pin('D9'))
#servo = Servo(PWM("P0"), offset=0)
#servo.set_angle(20)

# init Raspberry Pi Camera
#camera = picamera.PiCamera()
#camera.resolution = (224, 224)  # ML model expects 224x224 image

# specify paths to local file assets
path_to_labels = "birds-label.txt"
path_to_model = "birds-model.tflite"
path_to_image = "static/"
sleep_interval = 3
detection_interval = 60

# confidence threshold at which you want to be notified of a new bird
prob_threshold = 0.4

db = sqlite3.connect('Weather.db')
cur = db.cursor()

#Majority of code from Tensorflow Lite Example
def object_detection():
	#static variables
	model = "efficientdet_lite0.tflite"
	camera_id = 0
	width = 640
	height = 480
	num_threads = 4
	enable_edgetpu = False

	# Variables to calculate FPS
	counter, fps = 0, 0
	start_time = time.time()

	# Start capturing video input from the camera
	cap = cv2.VideoCapture(camera_id)
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

	# Visualization parameters
	row_size = 20  # pixels
	left_margin = 24  # pixels
	text_color = (0, 0, 255)  # red
	font_size = 1
	font_thickness = 1
	fps_avg_frame_count = 10

	# Initialize the object detection model
	options = ObjectDetectorOptions(
		num_threads=num_threads,
		score_threshold=0.3,
		max_results=3,
		enable_edgetpu=enable_edgetpu)
	
	detector = ObjectDetector(model_path=model, options=options)

	# Continuously capture images from the camera and run inference
	while cap.isOpened():
		success, image = cap.read()
		if not success:
			sys.exit(
				'ERROR: Unable to read from webcam. Please verify your webcam settings.'
			)

		counter += 1
		image = cv2.flip(image, 1)

		# Run object detection estimation using the model.
		detections = detector.detect(image)
		object_categories = [detection.categories for detection in detections]
		for categories in object_categories:
			for category in categories:
				if category.label == "bird":
					# INSERT command to stop and sleep for 3 seconds.
					print("Bird detected now!")
					#time.sleep(detection_interval)
					
					weather_details = get_weather()
					time_recorded = datetime.now()
					file_name = str(uuid.uuid1())+'.jpg'

					cv2.imwrite(path_to_image+file_name, image)
					store_weather_details(weather_details, time_recorded, file_name)
				else:
					print("I see an object:", category.label)
					

		# Draw keypoints and edges on input image
		# image = utils.visualize(image, detections)

		# Calculate the FPS
		# if counter % fps_avg_frame_count == 0:
		# 	end_time = time.time()
		# 	fps = fps_avg_frame_count / (end_time - start_time)
		# 	start_time = time.time()

		# Show the FPS
		# fps_text = 'FPS = {:.1f}'.format(fps)
		# text_location = (left_margin, row_size)
		# cv2.putText(image, fps_text, text_location, cv2.FONT_HERSHEY_PLAIN,
		# 			font_size, text_color, font_thickness)

		# Stop the program if the ESC key is pressed.
		if cv2.waitKey(1) == 27:
			break
		#cv2.imshow('object_detector', image) #UNCOMMENT TO SEE VIDEO

		time.sleep(sleep_interval)

	cap.release()
	cv2.destroyAllWindows()

def check_for_bird():
    """ is there a bird at the feeder? """
    labels = load_labels()
    interpreter = Interpreter(path_to_model)
    interpreter.allocate_tensors()
    _, height, width, _ = interpreter.get_input_details()[0]['shape']

    #camera.start_preview()
    #time.sleep(2)  # give the camera 2 seconds to adjust light balance
    #camera.capture(path_to_image)
    image = Image.open(path_to_image)
    results = classify_image(interpreter, image)
    label_id, prob = results[0]
    # print("bird: " + labels[label_id])
    # print("prob: " + str(prob))
    #camera.stop_preview()

    if prob > prob_threshold:
        bird = labels[label_id]
        bird = bird[bird.find(",") + 1:]
        prob_pct = str(round(prob * 100, 1)) + "%"

def store_weather_details(weather_details, time_recorded, image_file_name):
	cur.execute("INSERT INTO Weather (temperature, humidity, pressure, description, wind, date, image) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}' )".format(weather_details['temperature'], weather_details['humidity'], weather_details['pressure'], weather_details['description'], weather_details['wind'], time_recorded, image_file_name))
	db.commit()
	#cur.execute( "INSERT INTO Weather VALUES ('%s', '%s', '%s', '%s', '%s', '%s' )" % weather_details['temperature'], weather_details['humidity'], weather_details['pressure'], weather_details['description'], weather_details['wind'], 'datetime("now")')

def get_weather():

	print('called get_weather...') #CREATE TABLE Weather(id INTEGER PRIMARY KEY AUTOINCREMENT, temperature REAL, humidity REAL, pressure REAL, description STRING, wind REAL, date DATETIME);
	
	API_KEY = '61bcb381201c040cc12527cf0d6a5228'
	CITY_NAME = 'WEST BEDFORD'
	weather_details = {}
	#LAT = 
	#LONG = 
	#url = 'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}'.format()

	# base_url variable to store url
	base_url = "http://api.openweathermap.org/data/2.5/weather?"

	complete_url = base_url + "appid=" + API_KEY + "&q=" + CITY_NAME

	print('calling url:', complete_url)

	# get method of requests module
	# return response object
	response = requests.get(complete_url)
	
	# json method of response object
	# convert json format data into
	# python format data
	x = response.json()
	
	# Now x contains list of nested dictionaries
	# Check the value of "cod" key is equal to
	# "404", means city is found otherwise,
	# city is not found
	if x["cod"] != "404":
	
		# store the value of "main"
		# key in variable y
		y = x["main"]
	
		# store the value corresponding
		# to the "temp" key of y
		weather_details['temperature'] = 1.8*(y["temp"]-273) + 32
	
		# store the value corresponding
		# to the "pressure" key of y
		weather_details['pressure'] = y["pressure"]
	
		# store the value corresponding
		# to the "humidity" key of y
		weather_details['humidity'] = y["humidity"]

		wind = x["wind"]
		weather_details['wind'] = wind["speed"]
	
		# store the value of "weather"
		# key in variable z
		z = x["weather"]
	
		# store the value corresponding
		# to the "description" key at
		# the 0th index of z
		weather_details['description'] = z[0]["description"]
	
	else:
		print(" City Not Found ")

	return weather_details


def load_labels():
    """ load labels for the ML model from the file specified """
    with open(path_to_labels, 'r') as f:
        return {i: line.strip() for i, line in enumerate(f.readlines())}


def set_input_tensor(interpreter, image):
    tensor_index = interpreter.get_input_details()[0]['index']
    input_tensor = interpreter.tensor(tensor_index)()[0]
    input_tensor[:, :] = image


def classify_image(interpreter, image, top_k=1):
    """ return a sorted array of classification results """
    set_input_tensor(interpreter, image)
    interpreter.invoke()
    output_details = interpreter.get_output_details()[0]
    output = np.squeeze(interpreter.get_tensor(output_details['index']))

    # if model is quantized (uint8 data), then dequantize the results
    if output_details['dtype'] == np.uint8:
        scale, zero_point = output_details['quantization']
        output = scale * (output - zero_point)

    ordered = np.argpartition(-output, top_k)
    return [(i, output[i]) for i in ordered[:top_k]]

def printTime():
	now = datetime.now()
	current_time = now.strftime("%H:%M:%S")
	print("Current Time =", current_time)

if __name__ == "__main__":
	try:
		object_detection()
	finally:
		print("stopping")

